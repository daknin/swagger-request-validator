//CHECKSTYLE:OFF // HideUtilityClassConstructor: this is no utility class
package com.atlassian.oai.validator.example.async;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(RestServiceApplication.class, args);
    }
}
//CHECKSTYLE:ON
